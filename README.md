# dGame - Canal de Youtube y Twitch

Gracias a Bitbucket, podremos mostrar y compartir todo el 
c�digo libre, que mostrados en los "streamings" o 
del canal de "[Youtube](https://www.youtube.com/channel/UCMWNwjk3gmNhFD1fi4EyLjg?view_as=subscriber)" y "[Twitch](https://www.twitch.tv/dgame_es)" sobre el desarrollo de un videojuego.



## 1 - Presentaci�n: Creaci�n de un videojuego

Realizar un videojuego no es tarea sencilla, se requiere de muchos 
conocimientos como: modelado 3D, iluminaci�n, sonido, efectos especiales,
programadores de c�digo, etc.

Por ello dejaremos todo el c�digo del desarrollo a 
disposici�n y de libre uso.

- Ver el contenido en Youtube. [[ Ver video ]](https://dgame.es/video/01_presentacion)

##2 - Creaci�n autom�tica de proyectos

Creamos un metodo autom�tico para crear la estructura de carpetas 
de nuestro proyecto. Este metodo podremos extrapolarlo a cualquier 
proyecto.

- Ver el contenido en Youtube. [[ Ver video ]](https://dgame.es/video/02_sistema_de_carpetas)
- Ver el c�digo completo. [[ Ver c�digo ]](https://dgame.es/code/02_sistema_de_carpetas)

##3 - Sistema de traducci�n de la UI 

Mostramos como implemetar un metodo para traducir los componentes 
de la interfaz gr�fica. Us�remos el sistema de datos **JSON**.

- Ver el contenido en Youtube. [[ Ver video ]](https://dgame.es/video/03_sistema_traducion)
- Ver el c�digo completo. [[ Ver c�digo ]](https://dgame.es/code/03_sistema_traducion)

##4 - Codificaci�n criptogr�fica 
 
 > Pr�ximamente

- AES
- DES3
- RSA

##5 - Sistema de guardado de datos

> Pr�ximamente

##6 - Sistema de inventario

> Pr�ximamente


